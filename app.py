import re
import sys
import asyncio
import argparse
import urllib
import collections

import aiohttp
import async_timeout
from bs4 import BeautifulSoup
import lxml
from lxml.html.clean import Cleaner
import nltk


ARGS = argparse.ArgumentParser(description="Web crawler")
ARGS.add_argument(
    '--iocp', action='store_true', dest='iocp',
    default=False, help='Use IOCP event loop (Windows only)')
ARGS.add_argument(
    '--uvloop', action='store_true', dest='uvloop',
    default=False, help='Use uvloop event loop')
ARGS.add_argument(
    '--select', action='store_true', dest='select',
    default=False, help='Use Select event loop instead of default')
ARGS.add_argument(
    'roots', nargs='*',
    default=[], help='Root URL (may be repeated)')
ARGS.add_argument(
    '--regex', action='store', metavar='REGEX',
    help='Find by regex')
ARGS.add_argument(
    '--exclude', action='store', metavar='REGEX',
    help='Exclude matching URLs')
ARGS.add_argument(
    '--max_redirect', action='store', type=int, metavar='N',
    default=10, help='Limit redirection chains (for 301, 302 etc.)')
ARGS.add_argument(
    '--max_tries', action='store', type=int, metavar='N',
    default=4, help='Limit retries on network errors')
ARGS.add_argument(
    '--max_tasks', action='store', type=int, metavar='N',
    default=100, help='Limit concurrent connections')
ARGS.add_argument(
    '--max_pool', action='store', type=int, metavar='N',
    default=100, help='Limit connection pool size')
ARGS.add_argument(
    '--timeout', action='store', type=int, metavar='N',
    default=15, help='Limit working time')
ARGS.add_argument(
    '--most_common', action='store', type=int, metavar='N',
    default=100, help='Limit most common results')
ARGS.add_argument(
    '-debug', action='store_true', dest='debug',
    default=False, help='Enable debug mode')
ARGS.add_argument(
    '-v', '--verbose', action='count', dest='level',
    default=1, help='Verbose logging (repeat for more verbose)')
ARGS.add_argument(
    '-q', '--quiet', action='store_const', const=0, dest='level',
    default=1, help='Quiet logging (opposite of --verbose)')


LINK_RE = re.compile(r'(?i)href=["\']?([^\s"\'<>]+)')


class Logger:
    def __init__(self, level):
        self.level = level

    def _log(self, n, args):
        if self.level >= n:
            print(*args, file=sys.stderr, flush=True)

    def log(self, n, *args):
        self._log(n, args)

    def __call__(self, n, *args):
        self._log(n, args)


async def fetch(client, url, governor, timeout):
    #async with governor:
    async with client.get(url, timeout=timeout) as response:
        body = await response.text()
        return body


async def worker(client, input_queue, output_queue, regex, exclude, governor, timeout, text_only=True):
    url = await input_queue.get()
    print('url:', url)
    try:
        body = await fetch(client, url, governor, timeout)
        cleaner = Cleaner()
        cleaner.javascript = True
        cleaner.style = True

        if text_only:
            soup = BeautifulSoup(cleaner.clean_html(body), 'html.parser')
            body_text = soup.get_text()
        else:
            body_text = body

        items = re.findall(regex, body_text)
        for item in items:
            await output_queue.put(item)

        page_urls = set(LINK_RE.findall(body))

        for new_url in page_urls:
            new_url = unescape(new_url)
            if '^(?http|https)://' not in new_url:
                new_url = urllib.parse.urljoin(url, new_url)
                new_url, frag = urllib.parse.urldefrag(new_url)
            new_url = new_url.replace('https', 'http')
            if not new_url == url:
                exclude_url = bool(re.match(exclude, new_url)) if exclude else False
                if not exclude_url:
                    await input_queue.put(new_url)
    except Exception as e:
        print(e)


async def init(loop, timeout, amount, max_tasks, input_queue, output_queue, regex, exclude, governor):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
    }
    async with aiohttp.ClientSession(loop=loop, headers=headers) as client:
        jobs = [
            asyncio.ensure_future(worker(client, input_queue, output_queue, regex, exclude, governor, timeout))
            for i in range(max_tasks)
        ]
        await asyncio.gather(*jobs)


async def report(output_queue, most_common):
    results = []
    for i in range(output_queue.qsize()):
        item = await output_queue.get()
        tokenized = nltk.word_tokenize(item.lower())
        if tokenized:
            results.append(tokenized[0])

    counter = collections.Counter(results)
    print([item for item, _ in counter.most_common(most_common)])


async def run(loop, input_queue, output_queue, roots, regex, exclude, governor, max_tasks, timeout, most_common, log=None):
    for root in roots:
        await input_queue.put(root)
    amount = len(roots)
    await init(loop, timeout, amount, max_tasks, input_queue, output_queue, regex, exclude, governor)
    await asyncio.shield(report(output_queue, most_common))

ESCAPES = [('quot', '"'),
           ('gt', '>'),
           ('lt', '<'),
           ('amp', '&')  # Must be last.
           ]


def unescape(url):
    """Turn &amp; into &, and so on.
    This is the inverse of cgi.escape().
    """
    for name, char in ESCAPES:
        url = url.replace('&' + name + ';', char)
    return url


def fix_url(url):
    """Prefix a schema-less URL with http://."""
    if '://' not in url:
        url = 'http://' + url
    return url


def main():
    args = ARGS.parse_args()
    if not args.roots:
        print('Use --help for command line help')
        return

    log = Logger(args.level)

    if args.iocp:
        from asyncio.windows_events import ProactorEventLoop
        loop = ProactorEventLoop()
        asyncio.set_event_loop(loop)
    elif args.select:
        loop = asyncio.SelectorEventLoop()
        asyncio.set_event_loop(loop)
    elif args.uvloop:
        import uvloop
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        loop = asyncio.get_event_loop()
    else:
        loop = asyncio.get_event_loop()

    loop.set_debug(args.debug)

    roots = {fix_url(root) for root in args.roots}
    regex = args.regex
    exclude = args.exclude

    max_tasks = args.max_tasks
    max_pool = args.max_pool
    timeout = args.timeout
    most_common = args.most_common

    input_queue = asyncio.Queue()
    output_queue = asyncio.Queue()
    governor = asyncio.locks.Semaphore(max_pool)

    loop.run_until_complete(
        run(
            loop=loop,
            input_queue=input_queue,
            output_queue=output_queue,
            governor=governor,
            max_tasks=max_tasks,
            most_common=most_common,
            roots=roots,
            regex=regex,
            exclude=exclude,
            log=log,
            timeout=timeout,
        )
    )

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
